package eu.telecomnancy;

import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.ConsoleUI;

public class StateApp {

    public static void main(String[] args) {
        ISensor sensor = new StateTemperatureSensor();
        new ConsoleUI(sensor);
    }
}