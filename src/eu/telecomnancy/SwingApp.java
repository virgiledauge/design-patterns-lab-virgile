package eu.telecomnancy;

import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
        new MainWindow(sensor);
    }
}
