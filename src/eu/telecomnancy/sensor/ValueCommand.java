package eu.telecomnancy.sensor;

public class ValueCommand implements ICommand {
  	private ISensor sensor;
  	public ValueCommand(ISensor sensor) {
      this.sensor = sensor;
   	}

   	public void execute(){
   	try{
      this.sensor.getValue();
   	}catch (SensorNotActivatedException e){
   		e.printStackTrace();
   	}
  	}
}	
