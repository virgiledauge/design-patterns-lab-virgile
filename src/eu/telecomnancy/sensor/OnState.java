package eu.telecomnancy.sensor;

import java.util.Random;

public class OnState implements IState{
    private double value = 0;
    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus(){
        return true;
    }
    
    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */

    public void update() throws SensorNotActivatedException{
        value = (new Random()).nextDouble() * 100;
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException{
        return value;
    }
}