package eu.telecomnancy.sensor;

public class UpdateCommand implements ICommand {
   private ISensor sensor;
   public UpdateCommand(ISensor sensor) {
      this.sensor = sensor;
   }
   public void execute(){
   	try{
      this.sensor.update();
   	}catch (SensorNotActivatedException e){
   		e.printStackTrace();
   	}
   }
}