package eu.telecomnancy.sensor;

public class OffState implements IState{
    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus(){
        return false;
    }
    
    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */

    public void update() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
}