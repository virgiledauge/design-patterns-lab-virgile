package eu.telecomnancy.sensor;

public abstract class SensorDecorator implements ISensor {
   protected ISensor decoratedSensor;

   public SensorDecorator(ISensor decoratedSensor){
      	this.decoratedSensor = decoratedSensor;
   }
   public void on(){
   		this.decoratedSensor.on();
   }

    /**
     * Disable the sensor.
     */
    public void off(){
    	this.decoratedSensor.off();
    }

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus(){
    	return this.decoratedSensor.getStatus();
    }

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException{
    	this.decoratedSensor.update();
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public abstract double getValue() throws SensorNotActivatedException;
}