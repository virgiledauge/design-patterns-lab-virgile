package eu.telecomnancy.sensor;

public class OnCommand implements ICommand {
   	private ISensor sensor;
   	public OnCommand(ISensor sensor) {
      	this.sensor = sensor;
   	}
   	public void execute(){
      	this.sensor.on();
   	}
}