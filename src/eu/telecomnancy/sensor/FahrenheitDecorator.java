package eu.telecomnancy.sensor;


public class FahrenheitDecorator extends SensorDecorator{
	public FahrenheitDecorator(ISensor decoratedSensor){
      	super(decoratedSensor);
   	}
	/**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException{
    	return this.decoratedSensor.getValue()*1.8+32;
    }
}