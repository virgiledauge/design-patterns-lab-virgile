package eu.telecomnancy.sensor;
import java.util.Observable;

public class IAdapter extends Observable implements ISensor{
    private double value;
    private LegacyTemperatureSensor _LegTempSens;
    public IAdapter(LegacyTemperatureSensor LegTempSens){
        _LegTempSens = LegTempSens;
        value = 0;
    }
    /**
     * Enable the sensor.
     */
    public void on(){
        if(!_LegTempSens.getStatus()){
            _LegTempSens.onOff();
            setChanged();
            notifyObservers(value);
        }
    }
    /**
     * Disable the sensor.
     */
    public void off(){
        if(_LegTempSens.getStatus()){
            _LegTempSens.onOff();
        }
    }
    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus(){
        return _LegTempSens.getStatus();
    }

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException{
        value = _LegTempSens.getTemperature();
        setChanged();
        notifyObservers(value);
    }
    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException{
        return value;
    }

}