package eu.telecomnancy.sensor;

public class OffCommand implements ICommand {

   private ISensor sensor;
   public OffCommand(ISensor sensor) {
      this.sensor = sensor;
   }
   public void execute(){
      this.sensor.off();
   }
}