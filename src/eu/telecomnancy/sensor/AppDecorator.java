package eu.telecomnancy;

import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.*;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:18
 */
public class AppDecorator {
    public static void main(String[] args) {
    	/*
        ISensor sensor = new FahrenheitDecorator(new TemperatureSensor());
        new ConsoleUI(sensor);
        */
        ISensor sensor1 = new RoundDecorator(new TemperatureSensor());
        new ConsoleUI(sensor1);
    }
}
