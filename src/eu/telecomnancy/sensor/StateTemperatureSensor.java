package eu.telecomnancy.sensor;

import java.util.Random;
import java.util.Observable; 

public class StateTemperatureSensor extends Observable implements ISensor {

    IState state;
    
    @Override
    public void on() {
        state = new OnState();
    }

    @Override
    public void off() {
        state = new OffState();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();
        setChanged();
        notifyObservers();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
    }

}
