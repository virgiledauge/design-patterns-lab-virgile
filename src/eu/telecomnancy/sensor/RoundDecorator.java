package eu.telecomnancy.sensor;

public class RoundDecorator extends SensorDecorator {
	public RoundDecorator(ISensor decoratedSensor){
      	super(decoratedSensor);
   	}
	/**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException{
    	return Math.round(this.decoratedSensor.getValue());
    }
}