package eu.telecomnancy;

import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.MainWindow;

public class SwingAdapterApp {

    public static void main(String[] args) {
        ISensor sensor = new IAdapter(new LegacyTemperatureSensor());
        new MainWindow(sensor);
    }

}
