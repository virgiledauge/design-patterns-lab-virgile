package eu.telecomnancy;

import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        ISensor sensor = new IAdapter(new LegacyTemperatureSensor());
        new ConsoleUI(sensor);
    }
}